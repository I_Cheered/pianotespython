import pygame 
import math
import time
import pygame.midi 
import os
from random import choice
import pickle

pygame.midi.init()


#The following function SHOULD be in PyGame but isn't. It returns the required font size to make a text be a certain size in a certain dimension (width or height). 

def getFontSizeBasedOnSize(text, size, dimension):
    dim = 0
    if dimension == "height":
        dim = 1
    color = (255, 255, 255)
    fontSize = 1
    textSize = [0, 0]
    while textSize[dim] < size:
        fontSize = fontSize + 1
        font = pygame.font.Font("Marlboro.ttf", fontSize)
        surface = font.render(text, True, color)
        textSize = surface.get_size()
    return fontSize

class Position:
    def __init__(self, x, y):
            self.x = x
            self.y = y

class Size:
    def __init__(self, width, height):
        self.width = width
        self.height = height

class Text():
    def __init__(self, screen, text, fontSize, x, y):
        self.text = text
        self.screen = screen
        self.font = pygame.font.Font("Marlboro.ttf", fontSize)
        self.color = (255, 255, 255)
        #Maybe add dynamic font sizing for fitting on any screen size
        #(Forloop that adjusts fontsize to get it closest to a certain desired textheight)
        self.surface = self.font.render(text, True, self.color)
        
        self.textSize = self.surface.get_size()
        self.position = Position(int(math.ceil(x - self.textSize[0]/2)), int(math.ceil(y - self.textSize[1]/2)))
        self.size = Size(self.textSize[0], self.textSize[1])
        self.draw()
    
    def draw(self):
        self.surface = self.font.render(self.text, True, self.color)
        self.screen.blit(self.surface, dest=(self.position.x,self.position.y))

class Rectangle():
    def __init__(self, screen, x, y, width, height):
        self.padding = screen.get_size()[0] / 50
        self.lineWidth = 3
        self.color = (255, 255, 255)
        self.backgroundcolor = (0, 0, 0)
        self.screen = screen
        self.position = Position(x - width/2  - self.padding/2, y - height/2 - self.padding/2)
        self.size = Size(width + self.padding, height + self.padding)
        self.rect = [math.ceil(self.position.x), math.ceil(self.position.y), math.ceil(self.size.width), math.ceil(self.size.height)]

    def draw(self):
        pygame.draw.rect(self.screen, self.color, self.rect, self.lineWidth)
        pygame.draw.rect(self.screen, self.backgroundcolor, self.rect, 0)
        

class Button():
    def __init__(self, screen, text, fontSize, x, y):
        self.hover = False
        self.text = Text(screen, text, fontSize, x, y)
        self.rectangle = Rectangle(screen, x, y, self.text.size.width, self.text.size.height)
        self.topLeft = [self.rectangle.position.x, self.rectangle.position.y]
        self.botRight = [self.rectangle.position.x + self.rectangle.size.width, self.rectangle.position.y + self.rectangle.size.height]
        self.isSelected = False
        self.draw()
        
    def draw(self):
        if self.isSelected == True:
            self.rectangle.backgroundcolor = (50, 50, 50)
        self.rectangle.draw()
        self.text.draw()
    
    def mouseOn(self, pos):
        if pos[0] > self.topLeft[0] and pos[0] < self.botRight[0] and pos[1] > self.topLeft[1] and pos[1] < self.botRight[1]:
            return True
        else:
            return False
    
    def hoverOn(self, pos):
        self.hover = self.mouseOn(pos)
        if self.hover:
            self.rectangle.backgroundcolor = (100, 100, 100)
        else:
            self.rectangle.backgroundcolor = (0, 0, 0)

    def onClick(self):
        if self.hover == True: return True
        else: return False


class EventHandler(object):
    _instance = None
    def __new__(self):
        if not self._instance:
            self._instance = super(EventHandler, self).__new__(self)
            self.gameState = 0
            self.gameStateChanged = False
            self.gameOctaves = 0
            self.gameSpeed = 0
            self.gameAmount = 0
            self.gameCompleted = 0
            self.gameMistakes = 0
            self.gameEnharmonic = [1, 0, 0] #[Non-Enharmonic, Sharps, Flats]
            self.loopTime = 0
            self.levelID = [0,0]
            self.inGame = False
            self.inLevelSelect = False
            self.inMainMenu = False
            self.updateLevelData = False
            self.inputMethod = ""
            self.pressedKey = ""
            self.isPaused = False
            self.gameEnharmonicsClicked = False
        return self._instance
    def updateLevelID(self):
        self.levelID[0] = self.gameOctaves
        self.levelID[1] = self.gameSpeed

class UI:
    def __init__(self, screen):
        self.screen = screen
        self.screensize = screen.get_size()
        self.width = self.screensize[0]
        self.height = self.screensize[1]
        self.eventHandler = EventHandler()
    
    def isActive(self): #Virtual function to be overwritten by class
        pass

class MainUI(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.titleText = "PiaNotes"
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1.5 / 5, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 5

        self.inputMethodText = "Choose an input method"
        self.inputMethodFontSize = getFontSizeBasedOnSize(self.inputMethodText, self.height * 1 / 13, "height")
        self.inputMethodXpos = self.width / 2
        self.inputMethodYpos = self.height * 3.5 / 10
        
        self.enharmonicsText = "Choose enharmonics (Sharps/Flats)"
        self.enharmonicsFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 13, "height")
        self.enharmonicsXpos = self.width / 2
        self.enharmonicsYpos = self.height * 6 / 10

        self.buttonText = "Start"
        self.buttonFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width/2
        self.buttonYpos = self.height * 9 / 10
        
        self.keyboardText = "Keyboard"
        self.keyboardFontSize = getFontSizeBasedOnSize(self.keyboardText, self.height * 1 / 18, "height")
        self.keyboardXpos = self.width/2 - self.width / 12
        self.keyboardYpos = self.height*4.5/10

        self.pianoText = "Piano (MIDI)"
        self.pianoFontSize = getFontSizeBasedOnSize(self.pianoText, self.height * 1 / 18, "height")
        self.pianoXpos = self.width/2 + self.width / 12
        self.pianoYpos = self.height*4.5/10

        # self.audioText = "Audio (WIP)"
        # self.audioFontSize = getFontSizeBasedOnSize(self.audioText, self.height * 1 / 18, "height")
        # self.audioXpos = self.width/2 + self.width / 8
        # self.audioYpos = self.height*4.5/10

        self.vanillaText = "Vanilla"
        self.vanillaFontSize = getFontSizeBasedOnSize(self.vanillaText, self.height * 1 / 18, "height")
        self.vanillaXpos = self.width/2 - self.width / 10
        self.vanillaYpos = self.height*7/10

        self.sharpsText = "Sharps"
        self.sharpsFontSize = getFontSizeBasedOnSize(self.sharpsText, self.height * 1 / 18, "height")
        self.sharpsXpos = self.width/2
        self.sharpsYpos = self.height*7/10

        self.flatsText = "Flats"
        self.flatsFontSize = getFontSizeBasedOnSize(self.flatsText, self.height * 1 / 18, "height")
        self.flatsXpos = self.width/2 + self.width / 10
        self.flatsYpos = self.height*7/10

        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)
        self.inputMethodText = Text(self.screen, self.inputMethodText, self.inputMethodFontSize, self.inputMethodXpos, self.inputMethodYpos)
        self.enharmonicsText = Text(self.screen, self.enharmonicsText, self.enharmonicsFontSize, self.enharmonicsXpos, self.enharmonicsYpos)
        self.keyboardButton = Button(self.screen, self.keyboardText, self.keyboardFontSize, self.keyboardXpos, self.keyboardYpos)
        self.pianoButton = Button(self.screen, self.pianoText, self.pianoFontSize, self.pianoXpos, self.pianoYpos)
        #self.audioButton = Button(self.screen, self.audioText, self.audioFontSize, self.audioXpos, self.audioYpos)
        self.vanillaButton = Button(self.screen, self.vanillaText, self.vanillaFontSize, self.vanillaXpos, self.vanillaYpos)
        self.sharpsButton = Button(self.screen, self.sharpsText, self.sharpsFontSize, self.sharpsXpos, self.sharpsYpos)
        self.flatsButton = Button(self.screen ,self.flatsText, self.flatsFontSize, self.flatsXpos, self.flatsYpos)
        self.startButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)
        self.keyboardButton.isSelected = True
        self.vanillaButton.isSelected = True

    def draw(self):
        self.title.draw()
        self.inputMethodText.draw()
        self.enharmonicsText.draw()
        self.keyboardButton.draw()
        self.pianoButton.draw()
        self.vanillaButton.draw()
        self.sharpsButton.draw()
        self.flatsButton.draw()
        self.startButton.draw()
        #self.audioButton.draw()
    
    def isActive(self):
        self.eventHandler.inGame = False
        self.eventHandler.inLevelSelect = False
        self.eventHandler.inMainMenu = True
        pos = pygame.mouse.get_pos()
        self.startButton.hoverOn(pos)
        self.keyboardButton.hoverOn(pos)
        self.pianoButton.hoverOn(pos)
        #self.audioButton.hoverOn(pos)
        self.vanillaButton.hoverOn(pos)
        self.sharpsButton.hoverOn(pos)
        self.flatsButton.hoverOn(pos)
        if pygame.mouse.get_pressed()[0] == True:
            if self.startButton.onClick() == 1 and self.eventHandler.gameStateChanged == False:
                self.eventHandler.gameState += 1
                self.eventHandler.gameStateChanged = True
            if self.pianoButton.onClick() == 1:
                self.eventHandler.inputMethod = "Piano"
                self.pianoButton.isSelected = True
                self.keyboardButton.isSelected = False
            if self.keyboardButton.onClick() == 1:
                self.eventHandler.inputMethod = "Keyboard"
                self.keyboardButton.isSelected = True
                self.pianoButton.isSelected = False
            # if self.audioButton.onClick() == 1:
            #     self.eventHandler.inputMethod = "Audio"
            #     self.audioButton.isSelected = True
            #     self.pianoButton.isSelected = False
            #     self.keyboardButton.isSelected = False
            if self.eventHandler.gameEnharmonicsClicked == False:
                if self.vanillaButton.onClick() == 1:
                    self.eventHandler.gameEnharmonicsClicked = True
                    if self.vanillaButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[0] = 1
                        self.vanillaButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[1] == 1 or self.eventHandler.gameEnharmonic[2] == 1:
                        self.eventHandler.gameEnharmonic[0] = 0
                        self.vanillaButton.isSelected = False
                if self.sharpsButton.onClick() == 1:
                    self.eventHandler.gameEnharmonicsClicked = True
                    if self.sharpsButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[1] = 1
                        self.sharpsButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[0] == 1 or self.eventHandler.gameEnharmonic[2] == 1:
                        self.eventHandler.gameEnharmonic[1] = 0
                        self.sharpsButton.isSelected = False
                if self.flatsButton.onClick() == 1:
                    self.eventHandler.gameEnharmonicsClicked = True
                    if self.flatsButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[2] = 1
                        self.flatsButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[0] == 1 or self.eventHandler.gameEnharmonic[1] == 1:
                        self.eventHandler.gameEnharmonic[2] = 0
                        self.flatsButton.isSelected = False
        self.draw()

class StateController():
    def __init__(self):
        self.eventHandler = EventHandler()
    
    def callGameState(self, StateObjects):
        StateObjects[self.eventHandler.gameState].isActive()

class Level(Button):
    def __init__(self, screen, octaves, speed, amount, percentage, locked, x, y):
        self.x = x 
        self.y = y
        self.screen = screen
        self.octaves = octaves
        self.speed = speed
        self.amount = amount
        self.percentage = percentage
        self.locked = locked
        self.text = str(self.octaves+1) + " - " + str(self.speed) + " | " + str(self.percentage) + "%"
        self.fontSize = getFontSizeBasedOnSize(self.text, self.screen.get_size()[0] * 1 / 7, "width")
        self.saveData = [self.octaves, self.speed, self.amount, self.percentage, self.locked]
        self.highscore = 0
        self.updateText()
        
    
    def updateLockedColor(self):
        if self.locked == True:
            self.rectangle.color = (100, 100, 100)
            self.text.color = (100, 100, 100)
        if self.locked == False:
            self.rectangle.color = (255, 255, 255)
            self.text.c = (255, 255, 255)

    def updateText(self):
        if self.highscore == self.amount:
            self.text = str(self.octaves+1) + " - " + str(self.speed) + " | " + str(self.percentage) + "%"
        else:
            self.text = str(self.octaves+1) + " - " + str(self.speed) + " | " + str(self.highscore) + "/" + str(self.amount)
        super().__init__(self.screen, self.text, self.fontSize, self.x, self.y)
    
    def dataToVars(self):
        self.octaves = self.saveData[0]
        self.speed = self.saveData[1]
        self.amount = self.saveData[2]
        self.percentage = self.saveData[3]
        self.locked = self.saveData[4]
        self.updateText()

    def varsToData(self):
        self.saveData = [self.octaves, self.speed, self.amount, self.percentage, self.locked]
        self.updateText()

class DataIO():
    def __init__(self):
        self.eventHandler = EventHandler()
    
    def write(self, data):
        fileName = self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
        try:
            open(fileName, 'w').close()
        except:
            open(fileName, "a").close()
        f = open(fileName, "wb")
        pickle.dump(data, f)
        f.close()
        print("Data written to file")

    def read(self):
        fileName = self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
        f = open(fileName, "rb")
        data = pickle.load(f)
        f.close()
        print("Data read from file")
        return data

class LevelSelect(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.dataIO = DataIO()

        self.titleText = "Level Select"
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1 / 8, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 10
        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)

        self.buttonText = "Back"
        self.buttonFontSize = getFontSizeBasedOnSize(self.buttonText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width / 20
        self.buttonYpos = self.height / 10
        self.backButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)

        self.octaveRange = 4 #Even though there are 7 octaves, the middle 4 are used like 98% of the time
        self.speedRange = 6
        self.levelButtonArray = self.getLevelButtonArray(self.octaveRange, self.speedRange)
        self.levelButtonSaveDataArray = self.createLevelButtonSaveDataArray()
    
    def draw(self):
        self.title.draw()
        self.backButton.draw()
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                self.levelButtonArray[i][j].updateLockedColor()
                self.levelButtonArray[i][j].draw()
    
    def createLevelButtonSaveDataArray(self):
        verticalArray = []
        for i in range(self.octaveRange):
            horizontalArray = []
            for j in range(self.speedRange):
                horizontalArray.append(self.levelButtonArray[i][j].saveData)
            verticalArray.append(horizontalArray)
        return verticalArray
    
    def levelButtonDataToSave(self):
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                self.levelButtonSaveDataArray[i][j] = self.levelButtonArray[i][j].saveData
        self.dataIO.write(self.levelButtonSaveDataArray)

    def saveToLevelButtons(self):
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                self.levelButtonArray[i][j].saveData = self.levelButtonSaveDataArray[i][j]
                self.levelButtonArray[i][j].dataToVars()

    def getLevelButtonArray(self, maxOctaves, maxSpeed):
        octaveArray = []
        for i in range(maxOctaves):
            speedArray = []
            for j in range(maxSpeed): #FIX AMOUNT HERE
                speedArray.append(Level(self.screen, i, j, 15 + 15 * j, 0, True, self.width/10 + self.width * j / 5, self.height * 3 / 10 + self.height * i / 5))
            octaveArray.append(speedArray)
        octaveArray[0][0].locked = False
        levelButtonArray = octaveArray
        self.eventHandler.gameCompleted = 0
        self.eventHandler.gameMistakes = 0
        return levelButtonArray
        
    def isActive(self):
        ####################################################################################3
        self.eventHandler.inLevelSelect = True
        if self.eventHandler.inMainMenu == True:
            self.eventHandler.inMainMenu = False
            if os.path.exists(self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"):
                self.levelButtonSaveDataArray = self.dataIO.read()
                self.saveToLevelButtons()
            else:
                self.levelButtonArray = self.getLevelButtonArray(self.octaveRange, self.speedRange)
                self.levelButtonSaveDataArray = self.createLevelButtonSaveDataArray()
        if self.eventHandler.inGame == True:
            self.eventHandler.inGame = False
            self.levelButtonDataToSave()
        if self.eventHandler.updateLevelData == True:
            self.eventHandler.updateLevelData = False
            if self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].percentage < math.ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100):
                self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].percentage = math.ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100)
            if self.eventHandler.gameCompleted > self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].highscore:
                self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].highscore = self.eventHandler.gameCompleted
            if self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].percentage > 75 and self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].highscore == self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].amount:
                if self.eventHandler.levelID[1] == self.speedRange - 2:
                    self.levelButtonArray[self.eventHandler.levelID[0] + 1][0].locked = False
                else:
                    self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1] + 1].locked = False
            self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].updateText()
            for i in range(self.octaveRange):
                for j in range(self.speedRange):
                    self.levelButtonArray[i][j].varsToData()
            self.levelButtonDataToSave()            
        
        pos = pygame.mouse.get_pos()
        self.backButton.hoverOn(pos)
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                if self.levelButtonArray[i][j].locked == False:
                    self.levelButtonArray[i][j].hoverOn(pos)

        if pygame.mouse.get_pressed()[0] == True and self.eventHandler.gameStateChanged == False:
            if self.backButton.onClick() == 1:
                self.eventHandler.gameState -= 1
                self.eventHandler.gameStateChanged = True
            else:
                for i in range(self.octaveRange):
                    for j in range(self.speedRange):
                        if self.levelButtonArray[i][j].onClick() and self.eventHandler.gameStateChanged == False:
                            self.eventHandler.gameSpeed = self.levelButtonArray[i][j].speed
                            self.eventHandler.gameOctaves = self.levelButtonArray[i][j].octaves
                            self.eventHandler.gameAmount = self.levelButtonArray[i][j].amount
                            self.eventHandler.updateLevelID()

                            self.eventHandler.gameState += 1
                            self.eventHandler.inLevelSelect = False
                            self.eventHandler.gameStateChanged = True
        self.draw()

class StaticObjects():
    def __init__(self, screen):
        self.screen = screen
        self.topOffset = 100
        self.linesLeftOffset = 100
        self.bottomOffset = 100
        self.heightRange = screen.get_size()[1] - self.topOffset - self.bottomOffset
        self.lineSpacing = self.heightRange/26 #Total range is 27 notes, divide the range in 26
        self.treblecleff = pygame.transform.scale(pygame.image.load("treblecleff.png"), (self.linesLeftOffset- 15, int(10*self.lineSpacing)))
        self.basscleff = pygame.transform.scale(pygame.image.load("basscleff.png"), (self.linesLeftOffset - 15, int(5*self.lineSpacing)))
    
    def draw(self):
        for i in range(0, 5):
            pygame.draw.line(self.screen,(255,255,255),[self.linesLeftOffset , math.ceil(self.topOffset + (2*i+3)*self.lineSpacing)], [self.screen.get_size()[0],math.ceil(self.topOffset + (2*i+3)*self.lineSpacing)], 2)
        for i in range(0, 5):
            pygame.draw.line(self.screen,(255,255,255),[self.linesLeftOffset , math.ceil(self.topOffset + (2*i+15)*self.lineSpacing)], [self.screen.get_size()[0],math.ceil(self.topOffset + (2*i+15)*self.lineSpacing)], 2)
        pygame.draw.line(self.screen,(255,255,255),[self.linesLeftOffset + 10 , self.topOffset - 5], [self.linesLeftOffset + 10, self.screen.get_size()[1] - self.bottomOffset + 5], 6)
        self.screen.blit(self.treblecleff, [10, math.ceil(self.topOffset + 2*self.lineSpacing)])
        self.screen.blit(self.basscleff, [10, math.ceil(self.topOffset + 17*self.lineSpacing)])

class PlayScreen(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.screen = screen
        self.staticObjects = StaticObjects(self.screen)

        self.buttonText = "Back"
        self.buttonFontSize = getFontSizeBasedOnSize(self.buttonText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width / 10
        self.buttonYpos = self.height/10
        self.backButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)

        self.pauseText = "Pause"
        self.pauseFontSize = getFontSizeBasedOnSize(self.pauseText, self.height * 1 / 13, "height")
        self.pauseXpos = self.width * 9 / 10
        self.pauseYpos = self.height / 10
        self.pauseButton = Button(self.screen, self.pauseText, self.pauseFontSize, self.pauseXpos, self.pauseYpos)

        self.titleText = "Playing level " + str(self.eventHandler.gameOctaves) + " - " + str(self.eventHandler.gameSpeed)
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1 / 8, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 10
        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)

        self.progText = "Progress " + str(0) + " / " + str(self.eventHandler.gameAmount)
        self.progFontSize = getFontSizeBasedOnSize(self.progText, self.height * 1 / 10, "height")
        self.progXpos = self.width * 4 / 5
        self.progYpos = self.height * 9 / 10
        self.prog = Text(self.screen, self.progText, self.progFontSize, self.progXpos, self.progYpos)

        if self.eventHandler.gameCompleted + self.eventHandler.gameMistakes == 0:
            self.scoreText = "Accuracy: -- %"
        else: self.scoreText = "Accuracy: " + str(math.ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100)) + "%"
        self.scoreFontSize = getFontSizeBasedOnSize(self.scoreText, self.height * 1 / 10, "height")
        self.scoreXpos = self.width  / 5
        self.scoreYpos = self.height * 9 / 10
        self.score = Text(self.screen, self.scoreText, self.scoreFontSize, self.scoreXpos, self.scoreYpos)

        self.noteArrayClass = NoteArrayClass(self.screen, self.staticObjects.lineSpacing, self.staticObjects.topOffset)
        self.noteArray = [] #Temporary placeholder, updated when level starts
        
        self.ID = 0
        if self.eventHandler.inputMethod == "Piano":
            self.ID = 10
            self.stream = pygame.midi.Input(self.ID) #Fix this to include other forms of input (sound, keyboard)

    def drawMovables(self):
        for current in self.noteArray:
            if current.isCompleted != True:
                pygame.draw.rect(self.screen, (255, 255, 255), (math.ceil(current.x), math.ceil(current.y), math.ceil(current.width), math.ceil(current.height)))
                if current.enharmonic == "sharp":
                    pygame.draw.line(self.screen, (255, 255, 255), (math.ceil(current.x) -current.width/5, math.ceil(current.y) + current.height/5), (math.ceil(current.x) - current.width * 4 / 5, math.ceil(current.y) + current.height/5), 3) #horizontal top
                    pygame.draw.line(self.screen, (255, 255, 255), (math.ceil(current.x) -current.width/5, math.ceil(current.y) + current.height * 2/5), (math.ceil(current.x) - current.width * 4 / 5, math.ceil(current.y) + current.height * 2/5), 3) #horizontal bot
                    pygame.draw.line(self.screen, (255, 255, 255), (math.ceil(current.x) -current.width * 2/5, math.ceil(current.y) ), (math.ceil(current.x) -current.width * 2/5, math.ceil(current.y) + current.height * 3/5), 3) #vertical right
                    pygame.draw.line(self.screen, (255, 255, 255), (math.ceil(current.x) -current.width * 3/5, math.ceil(current.y) ), (math.ceil(current.x) -current.width * 3/5, math.ceil(current.y) + current.height * 3/5), 3) #vertical left
                if current.enharmonic == "flat":
                    pygame.draw.line(self.screen, (255, 255, 255), (math.ceil(current.x) - current.width * 4 / 5, math.ceil(current.y)), (math.ceil(current.x) - current.width * 4 / 5, math.ceil(current.y) + current.height * 3 / 5), 3)#Vertical line
                    rect = (math.ceil(current.x) - current.width * 4 / 5, math.ceil(current.y) + current.height * 2 / 5, current.width * 4 / 5, current.height * 3 / 5)
                    pygame.draw.ellipse(self.screen, (255, 255, 255), rect, 6)

    def getInput(self):
        if self.eventHandler.inputMethod == "Piano":
            if self.stream.poll():
                keyInput = self.stream.read(10)
                if not any([248, 0, 0, 0] in sl for sl in keyInput): #This line removes all the "empty" inputs that the piano sends
                    if not keyInput[0][0][2] == 0:
                        return keyInput[0][0][1]
                    else:
                        return 0
                else:
                    return 0
            else:
                return 0
        if self.eventHandler.inputMethod == "Keyboard":
            letterInput = ""
            pressedKeys = pygame.key.get_pressed()
            if pressedKeys[pygame.K_a]:
                letterInput = "a"
            elif pressedKeys[pygame.K_b]:
                letterInput = "b"
            elif pressedKeys[pygame.K_c]:
                letterInput = "c"
            elif pressedKeys[pygame.K_d]:
                letterInput = "d"
            elif pressedKeys[pygame.K_e]:
                letterInput = "e"
            elif pressedKeys[pygame.K_f]:
                letterInput = "f"
            elif pressedKeys[pygame.K_g]:
                letterInput = "g"

            if pressedKeys[pygame.K_RSHIFT] or pressedKeys[pygame.K_LSHIFT]:
                if letterInput != "":
                    letterInput += "sharp"
            elif pressedKeys[pygame.K_RALT] or pressedKeys[pygame.K_LALT]:
                if letterInput != "":
                    letterInput += "flat"

            if self.eventHandler.pressedKey == letterInput:
                return ""
            
            self.eventHandler.pressedKey = letterInput
            return letterInput

    def updateObjects(self, givenInput):
        for current in self.noteArray:
            if current.isCompleted != True and self.eventHandler.isPaused == False:
                current.x = current.x - (50 + 50 * self.eventHandler.gameSpeed) * self.eventHandler.loopTime 
                if current.x < self.staticObjects.linesLeftOffset:
                    self.eventHandler.updateLevelData = True
                    self.eventHandler.gameState -= 1
        for i in range(len(self.noteArray)):
            correct = False
            gotInput = False
            if self.noteArray[i].isFirst == True:
                if self.eventHandler.inputMethod == "Piano":
                    if givenInput != 0:
                        #print("Input: "  + str(givenInput))
                        gotInput = True
                        if givenInput == self.noteArray[i].val:
                            correct = True
                if self.eventHandler.inputMethod == "Keyboard":
                    if givenInput != "":
                        gotInput = True
                        #print("Input: "  + givenInput)
                        if len(givenInput) == 1 and self.noteArray[i].enharmonic == "":
                            if givenInput == self.noteArray[i].noteLetter:
                                correct = True
                        if len(givenInput) != 1 and self.noteArray[i].enharmonic != "":
                            if givenInput == self.noteArray[i].noteLetter + self.noteArray[i].enharmonic:
                                correct = True
                break

        if gotInput == True and correct == True:
            self.noteArray[i].isFirst = False 
            self.noteArray[((i+1)%len(self.noteArray))].isFirst = True 
            self.noteArray[i].isCompleted = True
            self.eventHandler.gameCompleted += 1
            if self.eventHandler.gameCompleted == self.eventHandler.gameAmount:
                self.eventHandler.updateLevelData = True
                self.eventHandler.gameState -= 1
        if gotInput == True and correct == False:
            self.eventHandler.gameMistakes += 1

    def isActive(self):
        if self.eventHandler.inGame == False:
            self.eventHandler.isPaused = False
            self.eventHandler.gameCompleted = 0
            self.eventHandler.gameMistakes = 0
            self.noteArray = self.noteArrayClass.getNoteArray(self.eventHandler.gameOctaves, self.eventHandler.gameAmount, self.eventHandler.gameEnharmonic)
            self.eventHandler.inGame = True
        retrieveInput = self.getInput()
        self.updateObjects(retrieveInput)

        pos = pygame.mouse.get_pos()
        self.backButton.hoverOn(pos)
        self.pauseButton.hoverOn(pos)

        if self.ID == 0 and self.eventHandler.inputMethod == "Piano":
            self.ID = 10
            self.stream = pygame.midi.Input(self.ID)

        if pygame.mouse.get_pressed()[0] == True and self.eventHandler.gameStateChanged == False:
            if self.backButton.onClick() == 1:
                self.eventHandler.gameState -= 1
                self.eventHandler.gameStateChanged = True
            if self.pauseButton.onClick() == 1:
                if self.eventHandler.isPaused == False:
                    self.eventHandler.isPaused = True
                else:
                    self.eventHandler.isPaused = False
                self.eventHandler.gameStateChanged = True
        self.draw()

    def updateLevel(self):
        self.titleText = "Playing level " + str(self.eventHandler.gameOctaves + 1) + " - " + str(self.eventHandler.gameSpeed)
        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)
        self.progText = "Progress " + str(self.eventHandler.gameCompleted) + " / " + str(self.eventHandler.gameAmount)
        self.prog = Text(self.screen, self.progText, self.progFontSize, self.progXpos, self.progYpos)
        if self.eventHandler.gameCompleted + self.eventHandler.gameMistakes == 0:
             self.scoreText = "Accuracy: -- %"
        else: self.scoreText = "Accuracy: " + str(math.ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100)) + "%"
        self.score = Text(self.screen, self.scoreText, self.scoreFontSize, self.scoreXpos, self.scoreYpos)

    def draw(self):
        self.updateLevel()
        self.title.draw()
        self.prog.draw()
        self.score.draw()
        self.backButton.draw()
        self.pauseButton.draw()
        self.staticObjects.draw()
        self.drawMovables()

class Note():
    def __init__(self, x, val, enharmonic, y, letter, size):
        self.x = x
        self.val = val
        self.enharmonic = enharmonic
        self.y = y
        self.noteLetter = letter
        self.width = size
        self.height = size
        self.isFirst = False
        self.isCompleted = False

class NoteArrayClass():
    def __init__(self, screen, noteSize, topOffset):
        self.screen = screen
        self.topOffset = topOffset
        self.size = noteSize

    def getNoteArray(self, octaves, amount, enharmonic):
        noteArray = []
        #counter = 0
        #notes = [36, 38, 40, 41, 43, 45, 47, 48, 50, 52, 53, 55, 57, 59, 60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79, 81, 83]
        #for i in range(amount):
        for i in range(amount):
            randomValue = self.randomNote(octaves, enharmonic)
            enharmonicType = self.getEnharmonic(randomValue, enharmonic)
            noteArray.append(Note(self.screen.get_size()[0] + 4 * i * self.size, randomValue, enharmonicType, self.getYpos(randomValue, enharmonicType), self.getNoteLetter(randomValue, enharmonicType), 2*self.size))
        noteArray[0].isFirst = True
        return noteArray

    def getEnharmonic(self, randomValue, enharmonic):
        blackKeys = [1, 4, 6, 9, 11]
        if (randomValue - 21) % 12 in blackKeys:
            possibleChoices = []
            if enharmonic[1] == 1:
                possibleChoices.append("sharp")
            if enharmonic[2] == 1:
                possibleChoices.append("flat")
            if len(possibleChoices) != 0:
                pick = choice(possibleChoices)
                return pick
            else: return ""
        else:
            return ""

    def randomNote(self, octaves, enharmonic):
        #Always include first octave (from middle c upwards) and extend if needed
        bottomRange = 60
        topRange = 71
        if octaves > 0:
            bottomRange = 48
        if octaves > 1:
            topRange = 83
        if octaves > 2:
            bottomRange = 36

        possibleKeys = []
        for i in range(bottomRange, topRange + 1):
            possibleKeys.append(i)
        
        pickedKey = choice(possibleKeys)
        if enharmonic[1] == 0 and enharmonic[2] == 0:
            blackKeys = [1, 4, 6, 9, 11]
            while (pickedKey - 21) % 12 in blackKeys: 
                pickedKey = choice(possibleKeys)
        return pickedKey
    
    def getNoteLetter(self, value, enharmonicType):
        letterArray = ["a", "", "b", "c", "", "d", "", "e", "f", "", "g", ""]
        normalizedValue = (value - 21) % 12
        blackKeys = [1, 4, 6, 9, 11]
        if normalizedValue in blackKeys:
            if enharmonicType == "sharp":
                normalizedValue -= 1
            if enharmonicType == "flat":
                normalizedValue = (normalizedValue + 1) % 12
        letter = letterArray[normalizedValue]
        return letter


    def getYpos(self, value, enharmonicType):
        normalizedValue = abs(value - 83) #Values now range from 0 to 83-36=47 (max)
        octave = math.floor(normalizedValue / 12) #Find octave as seen from top
        note = normalizedValue % 12 #Find the note in the octave 
        noteArray = [0, None, 1, None, 2, None, 3, 4, None, 5, None, 6]
        if noteArray[note] == None:
            if enharmonicType == "sharp":
                note = note + 1 #its seen from the top
            if enharmonicType == "flat":
                note = note - 1
        yPos = 0
        multiplier = 0
        if octave == 0:
            multiplier = noteArray[note] - 1
        else:
            multiplier = (octave * 7) - 1 + noteArray[note]
        yPos = self.topOffset + multiplier * self.size
        #yPos = yPos + 3 * self.size

        return math.ceil(yPos)


class Timer:
    def __init__(self):
        self._start_time = None

    def start(self):
        if self._start_time is not None:
            print("Timer is running. Use .stop() to stop it")

        self._start_time = time.perf_counter()

    def stop(self):
        if self._start_time is None:
            print("Timer is not running. Use .start() to start it")

        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        return elapsed_time

def getScreen():
    windowHeight = 1000
    windowWidth = 1900
    screen = pygame.display.set_mode((windowWidth, windowHeight))
    pygame.display.set_caption('PiaNotes!')
    return screen

def mainFunc():
    pygame.init()
    screen = getScreen()
    screen.fill((0, 0, 0))

    eventHandler = EventHandler()
    eventHandler.inputMethod = "Keyboard"
    eventHandler.gameEnharmonic = [1, 0, 0] #[Non-Enharmonics, Sharps, Flats]
    timer = Timer()
    stateController = StateController()

    mainScreen = MainUI(screen)
    levelSelectScreen = LevelSelect(screen)
    playScreen = PlayScreen(screen)

    StateObjects = [mainScreen, levelSelectScreen, playScreen]

    running = True
    while running:
        timer.start()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONUP:
                eventHandler.gameStateChanged = False
                eventHandler.gameEnharmonicsClicked = False

        screen.fill((0, 0, 0))
        stateController.callGameState(StateObjects)
        pygame.display.update()
        eventHandler.loopTime = timer.stop()

if __name__ == "__main__":
    mainFunc()