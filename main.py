import pygame 
import math
import time
import pygame.midi 
import os
from random import choice
import pickle
import tensorflow as tf 
import pyaudio 
import struct
import numpy as np


from resources.EventHandlerClass import *

from resources.MiscResources import *
from resources.HightLighterClass import *

from resources.MainScreen import *
from resources.LevelSelect import *
from resources.PlayScreen import *

pygame.midi.init()

class StateController():
    def __init__(self):
        self.eventHandler = EventHandler()
    
    def callGameState(self, StateObjects):
        StateObjects[self.eventHandler.gameState].isActive()

def mainFunc():
    pygame.init()
    screen = getScreen(1000, 1900, "Pianotes!")
    screen.fill((0, 0, 0))

    eventHandler = EventHandler()
    eventHandler.inputMethod = "Keyboard"
    eventHandler.gameEnharmonic = [1, 0, 0] #[Non-Enharmonics, Sharps, Flats]
    timer = Timer()
    stateController = StateController()

    mainScreen = MainUI(screen)
    levelSelectScreen = LevelSelect(screen)
    playScreen = PlayScreen(screen)

    eventHandler.initiateInputMethod = playScreen.initInputMethod

    StateObjects = [mainScreen, levelSelectScreen, playScreen]

    running = True
    while running:
        timer.start()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONUP:
                eventHandler.mouseWasPressed = False

        screen.fill((0, 0, 0))
        stateController.callGameState(StateObjects)
        pygame.display.update()
        
        eventHandler.loopTime = timer.stop()

if __name__ == "__main__":
    mainFunc()