import pyaudio
import wave
import numpy as np
import struct
import math

CHUNK = 1024 * 2             # samples per frame
FORMAT = pyaudio.paInt16     # audio format (bytes per sample?)
CHANNELS = 1                 # single channel for microphone
RATE = 44100                 # samples per second
DEVICE = 7

# use a Blackman window
window = np.blackman(CHUNK)
# open stream
p = pyaudio.PyAudio()

stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    output=True,
    frames_per_buffer=CHUNK,
    input_device_index = DEVICE
)

# read some data

# play stream and find the frequency of each chunk
while True:
    # write data out to the audio stream
    #stream.write(data)
    data = stream.read(CHUNK, exception_on_overflow = False)  
    # unpack the data and times by the hamming window
    #indata = np.array(wave.struct.unpack("%dh"%(len(data)/RATE),data))
    # Take the fft and square each value
    data_int = struct.unpack(str(2 * CHUNK) + 'B', data)
    fftData = abs(np.fft.rfft(data_int))**2
    # find the maximum
    absAverage = np.abs(fftData[0:CHUNK])
    freq = math.floor(np.argmax(absAverage[1:1024]) * RATE/CHUNK + 14)
    #which = fftData[1:].argmax() + 1
    # use quadratic interpolation around the max
    if freq != len(fftData)-1:
        y0,y1,y2 = np.log(fftData[freq-1:freq+2:])
        x1 = (y2 - y0) * .5 / (2 * y1 - y2 - y0)
        # find the frequency and output it
        thefreq = (freq+x1)*RATE/CHUNK
        print("The freq is %f Hz." % (thefreq))
    else:
        thefreq = freq*RATE/CHUNK
        print("The freq is %f Hz." % (thefreq))
    # read some more data
    #data = wf.readframes(chunk)
if data:
    stream.write(data)
stream.close()
p.terminate()