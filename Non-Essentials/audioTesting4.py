import pyaudio
import os
import struct
import numpy as np

from scipy.fftpack import fft 

import pygame.midi 
pygame.midi.init()
import math
# use this backend to display in separate Tk window

# constants
CHUNK = 1024 * 8           # samples per frame. I think this should be the amount of samples to cover the time it takes for 2 full cycles of the LOWEST frequency
FORMAT = pyaudio.paInt16     # audio format (bits per sample)
CHANNELS = 1                 # single channel for microphone
RATE = 48000              # samples per second, should be AT LEAST twice the desired highest frequency, higher is better.


for i in range(1, 20):

    DEVICEINDEX = i

    # pyaudio class instance
    try:
        p = pyaudio.PyAudio()

        # stream object to get data from microphone
        stream = p.open(
            format=FORMAT,
            channels=CHANNELS,
            rate=RATE,
            input=True,
            output=True,
            frames_per_buffer=CHUNK,
            input_device_index = DEVICEINDEX
        )
        data = stream.read(CHUNK, exception_on_overflow = False)
        break
    except:
        print("It is not " + str(DEVICEINDEX))

if DEVICEINDEX == 20:
    print("Couldn't find a viable device")
else:
    print("It is device number" + str(DEVICEINDEX))
    
while True:
    data = stream.read(CHUNK, exception_on_overflow = False)
    data_int = struct.unpack(str(2*CHUNK) + 'B', data)
    print(data_int[0])
