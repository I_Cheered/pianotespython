from math import ceil, floor
from random import choice
from resources.BaseUIClass import *
from pathlib import Path
import pyaudio
import tensorflow as tf
import struct
import numpy as np

class StaticObjects():
    def __init__(self, screen):
        self.screen = screen
        self.topOffset = floor(self.screen.get_size()[1] / 5)
        self.linesLeftOffset = floor(self.screen.get_size()[0] / 10)
        self.bottomOffset = floor(self.screen.get_size()[1] / 5)
        self.heightRange = screen.get_size()[1] - self.topOffset - self.bottomOffset
        self.lineSpacing = self.heightRange/26 #Total range is 27 notes, divide the range in 26
        root = Path(".")
        self.treblecleff = pygame.transform.scale(pygame.image.load(str(root/"resources"/"treblecleff.png")), (self.linesLeftOffset- 15, int(10*self.lineSpacing)))
        self.basscleff = pygame.transform.scale(pygame.image.load(str(root/"resources"/"basscleff.png")), (self.linesLeftOffset - 15, int(5*self.lineSpacing)))
    
    def draw(self):
        for i in range(0, 5):
            pygame.draw.line(self.screen,(255,255,255),[self.linesLeftOffset , ceil(self.topOffset + (2*i+3)*self.lineSpacing)], [self.screen.get_size()[0],ceil(self.topOffset + (2*i+3)*self.lineSpacing)], 2)
        for i in range(0, 5):
            pygame.draw.line(self.screen,(255,255,255),[self.linesLeftOffset , ceil(self.topOffset + (2*i+15)*self.lineSpacing)], [self.screen.get_size()[0],ceil(self.topOffset + (2*i+15)*self.lineSpacing)], 2)
        pygame.draw.line(self.screen,(255,255,255),[self.linesLeftOffset + 10 , self.topOffset - 5], [self.linesLeftOffset + 10, self.screen.get_size()[1] - self.bottomOffset + 5], 6)
        self.screen.blit(self.treblecleff, [10, ceil(self.topOffset + 17*self.lineSpacing)])

class PlayScreen(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.screen = screen
        self.staticObjects = StaticObjects(self.screen)

        self.buttonText = "Back"
        self.buttonFontSize = getFontSizeBasedOnSize(self.buttonText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width / 10
        self.buttonYpos = self.height/10
        self.backButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)

        self.titleText = "Playing level " + str(self.eventHandler.gameOctaves) + " - " + str(self.eventHandler.gameSpeed)
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1 / 8, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 10
        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)

        self.progText = "Progress " + str(0) + " / " + str(self.eventHandler.gameAmount)
        self.progFontSize = getFontSizeBasedOnSize(self.progText, self.height * 1 / 10, "height")
        self.progXpos = self.width * 4 / 5
        self.progYpos = self.height * 9 / 10
        self.prog = Text(self.screen, self.progText, self.progFontSize, self.progXpos, self.progYpos)

        if self.eventHandler.gameCompleted + self.eventHandler.gameMistakes == 0:
            self.scoreText = "Accuracy: -- %"
        else: self.scoreText = "Accuracy: " + str(ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100)) + "%"
        self.scoreFontSize = getFontSizeBasedOnSize(self.scoreText, self.height * 1 / 10, "height")
        self.scoreXpos = self.width  / 5
        self.scoreYpos = self.height * 9 / 10
        self.score = Text(self.screen, self.scoreText, self.scoreFontSize, self.scoreXpos, self.scoreYpos)

        self.noteArrayClass = NoteArrayClass(self.screen, self.staticObjects.lineSpacing, self.staticObjects.topOffset)
        self.noteArray = [] 

        self.previousNote = 0
        self.previousNoteRepetition = 0
        self.pianoIsActive = False
        self.audioIsActive = False

    def initInputMethod(self):
        if self.eventHandler.inputMethod == "Piano" and self.pianoIsActive == False:
            self.pianoIsActive = True
            self.ID = 3
            self.stream = pygame.midi.Input(self.ID)
            self.pianoIsActive = True


        if self.eventHandler.inputMethod == "Audio" and self.audioIsActive == False:
            self.CHUNK = 2048          
            FORMAT = pyaudio.paInt16     
            CHANNELS = 1                 
            self.RATE = 48000
            DEVICEINDEX = 11 #FIX THIS to have dynamic device indexing or something

            p = pyaudio.PyAudio()
            
            self.stream = p.open(
                format=FORMAT,
                channels=CHANNELS,
                rate=self.RATE,
                input=True,
                output=True,
                frames_per_buffer=self.CHUNK,
                input_device_index = DEVICEINDEX)
            self.ai = tf.keras.models.load_model("PitchDetectAI.model")

            self.audioIsActive = True

    def drawMovables(self):
        for current in self.noteArray:
            if current.isCompleted != True:
                pygame.draw.rect(self.screen, (255, 255, 255), (ceil(current.x), ceil(current.y), ceil(current.width), ceil(current.height)))
                if current.enharmonic == "sharp":
                    pygame.draw.line(self.screen, (255, 255, 255), (ceil(current.x) -current.width/5, ceil(current.y) + current.height/5), (ceil(current.x) - current.width * 4 / 5, ceil(current.y) + current.height/5), 3) #horizontal top
                    pygame.draw.line(self.screen, (255, 255, 255), (ceil(current.x) -current.width/5, ceil(current.y) + current.height * 2/5), (ceil(current.x) - current.width * 4 / 5, ceil(current.y) + current.height * 2/5), 3) #horizontal bot
                    pygame.draw.line(self.screen, (255, 255, 255), (ceil(current.x) -current.width * 2/5, ceil(current.y) ), (ceil(current.x) -current.width * 2/5, ceil(current.y) + current.height * 3/5), 3) #vertical right
                    pygame.draw.line(self.screen, (255, 255, 255), (ceil(current.x) -current.width * 3/5, ceil(current.y) ), (ceil(current.x) -current.width * 3/5, ceil(current.y) + current.height * 3/5), 3) #vertical left
                if current.enharmonic == "flat":
                    pygame.draw.line(self.screen, (255, 255, 255), (ceil(current.x) - current.width * 4 / 5, ceil(current.y)), (ceil(current.x) - current.width * 4 / 5, ceil(current.y) + current.height * 3 / 5), 3)#Vertical line
                    rect = (ceil(current.x) - current.width * 4 / 5, ceil(current.y) + current.height * 2 / 5, current.width * 4 / 5, current.height * 3 / 5)
                    pygame.draw.ellipse(self.screen, (255, 255, 255), rect, 6)

    def getInput(self):
        if self.eventHandler.inputMethod == "Piano":
            if self.stream.poll():
                keyInput = self.stream.read(10)
                if not any([248, 0, 0, 0] in sl for sl in keyInput): #This line removes all the "empty" inputs that the piano sends
                    if not keyInput[0][0][2] == 0:
                        return keyInput[0][0][1]
                    else:
                        return 0
                else:
                    return 0
            else:
                return 0
        if self.eventHandler.inputMethod == "Keyboard":
            letterInput = ""
            pressedKeys = pygame.key.get_pressed()
            if pressedKeys[pygame.K_a]:
                letterInput = "a"
            elif pressedKeys[pygame.K_b]:
                letterInput = "b"
            elif pressedKeys[pygame.K_c]:
                letterInput = "c"
            elif pressedKeys[pygame.K_d]:
                letterInput = "d"
            elif pressedKeys[pygame.K_e]:
                letterInput = "e"
            elif pressedKeys[pygame.K_f]:
                letterInput = "f"
            elif pressedKeys[pygame.K_g]:
                letterInput = "g"

            if pressedKeys[pygame.K_RSHIFT] or pressedKeys[pygame.K_LSHIFT]:
                if letterInput != "":
                    letterInput += "sharp"
            elif pressedKeys[pygame.K_RALT] or pressedKeys[pygame.K_LALT]:
                if letterInput != "":
                    letterInput += "flat"

            if self.eventHandler.pressedKey == letterInput:
                return ""
            
            self.eventHandler.pressedKey = letterInput
            return letterInput
        
        if self.eventHandler.inputMethod == "Audio":
            measuredData = struct.unpack(str(2 * self.CHUNK) + 'B', self.stream.read(self.CHUNK, exception_on_overflow = False))
            adjusted = tf.keras.utils.normalize(measuredData)
            prediction = self.ai.predict(adjusted)
            guessedValue = np.argmax(prediction)
            midiNumber = guessedValue + 35

            if midiNumber == self.previousNote:
                self.previousNoteRepetition += 1
                if self.previousNoteRepetition > 2:
                    return midiNumber
                else:
                    return 0
            else:
                self.previousNote = midiNumber
                self.previousNoteRepetition = 0
                return 0

    def updateObjects(self, givenInput):
        for current in self.noteArray:
            if current.isCompleted != True and self.eventHandler.isPaused == False:
                current.x = current.x - (self.screen.get_size()[0] / 40 + self.screen.get_size()[0] / 60 * self.eventHandler.gameSpeed) * self.eventHandler.loopTime 
                if current.x < self.staticObjects.linesLeftOffset:
                    self.eventHandler.updateLevelData = True
                    self.eventHandler.gameState -= 1
        for i in range(len(self.noteArray)):
            correct = False
            gotInput = False
            if self.noteArray[i].isFirst == True:
                if self.eventHandler.inputMethod == "Piano" or self.eventHandler.inputMethod == "Audio":
                    if givenInput != 0:
                        #print("Input: "  + str(givenInput))
                        gotInput = True
                        if givenInput == self.noteArray[i].val:
                            correct = True
                if self.eventHandler.inputMethod == "Keyboard":
                    if givenInput != "":
                        gotInput = True
                        #print("Input: "  + givenInput)
                        if len(givenInput) == 1 and self.noteArray[i].enharmonic == "":
                            if givenInput == self.noteArray[i].noteLetter:
                                correct = True
                        if len(givenInput) != 1 and self.noteArray[i].enharmonic != "":
                            if givenInput == self.noteArray[i].noteLetter + self.noteArray[i].enharmonic:
                                correct = True
                break

        if gotInput == True and correct == True:
            self.noteArray[i].isFirst = False 
            self.noteArray[((i+1)%len(self.noteArray))].isFirst = True 
            self.noteArray[i].isCompleted = True
            self.eventHandler.gameCompleted += 1
            if self.eventHandler.gameCompleted == self.eventHandler.gameAmount:
                self.eventHandler.updateLevelData = True
                self.eventHandler.gameState -= 1
        if gotInput == True and correct == False and self.eventHandler.inputMethod != "Audio":
            self.eventHandler.gameMistakes += 1

    def isActive(self):
        if self.eventHandler.inGame == False:
            self.eventHandler.inGame = True
            self.eventHandler.isPaused = False
            self.eventHandler.gameCompleted = 0
            self.eventHandler.gameMistakes = 0
            self.previousNote = 0
            self.previousNoteRepetition = 0
            self.noteArray = self.noteArrayClass.getNoteArray(self.eventHandler.gameOctaves, self.eventHandler.gameAmount, self.eventHandler.gameEnharmonic)

        pos = pygame.mouse.get_pos()

        if self.eventHandler.inWhatButton == False:
            self.backButton.hoverOn(pos)
            self.highLighter.whatButton.hoverOn(pos)
            retrieveInput = self.getInput()
            self.updateObjects(retrieveInput)
            if pygame.mouse.get_pressed()[0] == True and self.eventHandler.mouseWasPressed == False:
                self.eventHandler.mouseWasPressed = True
                if self.backButton.onClick() == 1:
                    self.eventHandler.gameState -= 1
                if self.highLighter.whatButton.onClick() == 1:
                    self.eventHandler.isPaused = True
                    self.eventHandler.inWhatButton = True
            self.draw()
        if self.eventHandler.inWhatButton == True:
            self.draw()
            if self.eventHandler.whatButtonStage == 0:
                text = ["Randomly generated notes will move from right to left","You can complete notes by pressing the letter/key", "that matches the first note on screen"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.noteArray[self.eventHandler.gameCompleted].x, self.noteArray[self.eventHandler.gameCompleted].y, self.noteArray[self.eventHandler.gameCompleted].width, self.noteArray[self.eventHandler.gameCompleted].height, text, pos, "center")
            if self.eventHandler.whatButtonStage == 1:
                text = ["But don't wait too long, if the first note reaches", "this bar you have to restart the level"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.staticObjects.linesLeftOffset, self.staticObjects.topOffset, 6, self.staticObjects.heightRange, text, pos, "center")
            if self.eventHandler.whatButtonStage == 2:
                text = ["This displays how many notes are left to complete", "As the levels get harder, this amount increases"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.progXpos - self.prog.surface.get_width() / 2, self.progYpos - self.prog.surface.get_height() / 2, self.prog.surface.get_width(), self.prog.surface.get_height(), text, pos, "center")
            if self.eventHandler.whatButtonStage == 3:
                text = ["Making mistakes decreases this percentage", "You need at least 75% to pass the level"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.scoreXpos - self.score.surface.get_width() / 2, self.scoreYpos - self.score.surface.get_height() / 2, self.score.surface.get_width(), self.score.surface.get_height(), text, pos, "center")
            if self.eventHandler.whatButtonStage == 4:
                self.eventHandler.inWhatButton = False 
                self.eventHandler.isPaused = False
                self.eventHandler.whatButtonStage = 0

    def updateLevel(self):
        self.titleText = "Playing level " + str(self.eventHandler.gameOctaves + 1) + " - " + str(self.eventHandler.gameSpeed)
        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)
        self.progText = "Progress " + str(self.eventHandler.gameCompleted) + " / " + str(self.eventHandler.gameAmount)
        self.prog = Text(self.screen, self.progText, self.progFontSize, self.progXpos, self.progYpos)
        if self.eventHandler.gameCompleted + self.eventHandler.gameMistakes == 0:
             self.scoreText = "Accuracy: -- %"
        else: self.scoreText = "Accuracy: " + str(ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100)) + "%"
        self.score = Text(self.screen, self.scoreText, self.scoreFontSize, self.scoreXpos, self.scoreYpos)

    def draw(self):
        self.updateLevel()
        self.title.draw()
        self.prog.draw()
        self.score.draw()
        self.backButton.draw()
        self.highLighter.whatButton.draw()
        self.staticObjects.draw()
        self.drawMovables()

class Note():
    def __init__(self, x, val, enharmonic, y, letter, size):
        self.x = x
        self.val = val
        self.enharmonic = enharmonic
        self.y = y
        self.noteLetter = letter
        self.width = size
        self.height = size
        self.isFirst = False
        self.isCompleted = False

class NoteArrayClass():
    def __init__(self, screen, noteSize, topOffset):
        self.screen = screen
        self.topOffset = topOffset
        self.size = noteSize

    def getNoteArray(self, octaves, amount, enharmonic):
        noteArray = []
        for i in range(amount):
            randomValue = self.randomNote(octaves, enharmonic)
            enharmonicType = self.getEnharmonic(randomValue, enharmonic)
            noteArray.append(Note(self.screen.get_size()[0] + 4 * i * self.size, randomValue, enharmonicType, self.getYpos(randomValue, enharmonicType), self.getNoteLetter(randomValue, enharmonicType), 2*self.size))
        noteArray[0].isFirst = True
        return noteArray

    def getEnharmonic(self, randomValue, enharmonic):
        blackKeys = [1, 4, 6, 9, 11]
        if (randomValue - 21) % 12 in blackKeys:
            possibleChoices = []
            if enharmonic[1] == 1:
                possibleChoices.append("sharp")
            if enharmonic[2] == 1:
                possibleChoices.append("flat")
            if len(possibleChoices) != 0:
                pick = choice(possibleChoices)
                return pick
            else: return ""
        else:
            return ""

    def randomNote(self, octaves, enharmonic):
        #Always include first octave (from middle c upwards) and extend if needed
        bottomRange = 60
        topRange = 71
        if octaves > 0:
            bottomRange = 48
        if octaves > 1:
            topRange = 83
        if octaves > 2:
            bottomRange = 36

        possibleKeys = []
        for i in range(bottomRange, topRange + 1):
            possibleKeys.append(i)
        
        pickedKey = choice(possibleKeys)
        if enharmonic[1] == 0 and enharmonic[2] == 0:
            blackKeys = [1, 4, 6, 9, 11]
            while (pickedKey - 21) % 12 in blackKeys: 
                pickedKey = choice(possibleKeys)
        return pickedKey
    
    def getNoteLetter(self, value, enharmonicType):
        letterArray = ["a", "", "b", "c", "", "d", "", "e", "f", "", "g", ""]
        normalizedValue = (value - 21) % 12
        blackKeys = [1, 4, 6, 9, 11]
        if normalizedValue in blackKeys:
            if enharmonicType == "sharp":
                normalizedValue -= 1
            if enharmonicType == "flat":
                normalizedValue = (normalizedValue + 1) % 12
        letter = letterArray[normalizedValue]
        return letter


    def getYpos(self, value, enharmonicType):
        normalizedValue = abs(value - 83) #Values now range from 0 to 83-36=47 (max)
        octave = floor(normalizedValue / 12) #Find octave as seen from top
        note = normalizedValue % 12 #Find the note in the octave 
        noteArray = [0, None, 1, None, 2, None, 3, 4, None, 5, None, 6]
        if noteArray[note] == None:
            if enharmonicType == "sharp":
                note = note + 1 #its seen from the top
            if enharmonicType == "flat":
                note = note - 1
        yPos = 0
        multiplier = 0
        if octave == 0:
            multiplier = noteArray[note] - 1
        else:
            multiplier = (octave * 7) - 1 + noteArray[note]
        yPos = self.topOffset + multiplier * self.size
        #yPos = yPos + 3 * self.size

        return ceil(yPos)