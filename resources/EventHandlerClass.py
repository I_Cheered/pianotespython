class EventHandler(object):
    _instance = None
    def __new__(self):
        if not self._instance:
            self._instance = super(EventHandler, self).__new__(self)
            self.gameState = 0
            self.gameStateChanged = False
            self.gameOctaves = 0
            self.gameSpeed = 0
            self.gameAmount = 0
            self.gameCompleted = 0
            self.gameMistakes = 0
            self.gameEnharmonic = [1, 0, 0] #[Non-Enharmonic, Sharps, Flats]
            self.loopTime = 0
            self.levelID = [0,0]
            self.inGame = False
            self.inLevelSelect = False
            self.inMainMenu = False
            self.updateLevelData = False
            self.inputMethod = ""
            self.pressedKey = ""
            self.isPaused = False
            self.inWhatButton = False 
            self.whatButtonStage = 0
            self.mouseWasPressed = False
            self.initiateInputMethod = "" #This will turn into a reference for the input initiate function (open audio input stream, open midi device) so that it can be called by the main menu object
            self.inputDeviceID = 0
            self.inputDeviceError = False
        return self._instance
    def updateLevelID(self):
        self.levelID[0] = self.gameOctaves
        self.levelID[1] = self.gameSpeed
