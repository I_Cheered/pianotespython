from resources.BaseUIClass import *
import pyaudio

class MainUI(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.titleText = "PiaNotes"
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1.5 / 5, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 5

        self.inputMethodText = "Choose an input method"
        self.inputMethodFontSize = getFontSizeBasedOnSize(self.inputMethodText, self.height * 1 / 13, "height")
        self.inputMethodXpos = self.width / 2
        self.inputMethodYpos = self.height * 3.5 / 10
        
        self.enharmonicsText = "Choose enharmonics"
        self.enharmonicsFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 13, "height")
        self.enharmonicsXpos = self.width / 2
        self.enharmonicsYpos = self.height * 6 / 10

        self.inputErrorText = "Could not open this audio device"
        self.inputErrorFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 20, "height")
        self.inputErrorXpos = self.width / 2
        self.inputErrorYpos = self.height * 19 / 20

        self.buttonText = "Start"
        self.buttonFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width/2
        self.buttonYpos = self.height * 17 / 20
        
        self.keyboardText = "Keyboard"
        self.keyboardFontSize = getFontSizeBasedOnSize(self.keyboardText, self.height * 1 / 18, "height")
        self.keyboardXpos = self.width/2 - self.width / 8
        self.keyboardYpos = self.height*4.5/10

        self.audioText = "Audio"
        self.audioFontSize = getFontSizeBasedOnSize(self.audioText, self.height * 1 / 18, "height")
        self.audioXpos = self.width/2
        self.audioYpos = self.height*4.5/10

        self.pianoText = "Piano (MIDI)"
        self.pianoFontSize = getFontSizeBasedOnSize(self.pianoText, self.height * 1 / 18, "height")
        self.pianoXpos = self.width/2 + self.width / 8
        self.pianoYpos = self.height*4.5/10

        self.vanillaText = "Vanilla"
        self.vanillaFontSize = getFontSizeBasedOnSize(self.vanillaText, self.height * 1 / 18, "height")
        self.vanillaXpos = self.width/2 - self.width / 10
        self.vanillaYpos = self.height*7/10

        self.sharpsText = "Sharps"
        self.sharpsFontSize = getFontSizeBasedOnSize(self.sharpsText, self.height * 1 / 18, "height")
        self.sharpsXpos = self.width/2
        self.sharpsYpos = self.height*7/10

        self.flatsText = "Flats"
        self.flatsFontSize = getFontSizeBasedOnSize(self.flatsText, self.height * 1 / 18, "height")
        self.flatsXpos = self.width/2 + self.width / 10
        self.flatsYpos = self.height*7/10

        self.inputDeviceButtons = PickAudioDevice(screen)

        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)
        self.inputMethodText = Text(self.screen, self.inputMethodText, self.inputMethodFontSize, self.inputMethodXpos, self.inputMethodYpos)
        self.enharmonicsText = Text(self.screen, self.enharmonicsText, self.enharmonicsFontSize, self.enharmonicsXpos, self.enharmonicsYpos)
        self.inputErrorText = Text(self.screen, self.inputErrorText, self.inputErrorFontSize, self.inputErrorXpos, self.inputErrorYpos)
        self.keyboardButton = Button(self.screen, self.keyboardText, self.keyboardFontSize, self.keyboardXpos, self.keyboardYpos)
        self.pianoButton = Button(self.screen, self.pianoText, self.pianoFontSize, self.pianoXpos, self.pianoYpos)
        self.audioButton = Button(self.screen, self.audioText, self.audioFontSize, self.audioXpos, self.audioYpos)
        self.vanillaButton = Button(self.screen, self.vanillaText, self.vanillaFontSize, self.vanillaXpos, self.vanillaYpos)
        self.sharpsButton = Button(self.screen, self.sharpsText, self.sharpsFontSize, self.sharpsXpos, self.sharpsYpos)
        self.flatsButton = Button(self.screen ,self.flatsText, self.flatsFontSize, self.flatsXpos, self.flatsYpos)
        self.startButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)
        self.keyboardButton.isSelected = True
        self.vanillaButton.isSelected = True
        self.eventHandler.inputDeviceError = False

    def draw(self):
        self.title.draw()
        self.inputMethodText.draw()
        self.enharmonicsText.draw()
        self.keyboardButton.draw()
        self.pianoButton.draw()
        self.vanillaButton.draw()
        self.sharpsButton.draw()
        self.flatsButton.draw()
        self.startButton.draw()
        self.highLighter.whatButton.draw()
        #self.audioButton.draw()
        if self.eventHandler.inputMethod == "Piano" or self.eventHandler.inputMethod == "Audio":
            self.inputDeviceButtons.draw()
        if self.eventHandler.inputDeviceError == True:
            self.inputErrorText.draw()
    
    def isActive(self):
        self.eventHandler.inGame = False
        self.eventHandler.inLevelSelect = False
        self.eventHandler.inMainMenu = True
        pos = pygame.mouse.get_pos()
        if self.eventHandler.inWhatButton == False:
            self.startButton.hoverOn(pos)
            self.keyboardButton.hoverOn(pos)
            self.pianoButton.hoverOn(pos)
            #self.audioButton.hoverOn(pos)
            self.vanillaButton.hoverOn(pos)
            self.sharpsButton.hoverOn(pos)
            self.flatsButton.hoverOn(pos)
            self.highLighter.whatButton.hoverOn(pos)
            if self.eventHandler.inputMethod == "Piano" or self.eventHandler.inputMethod == "Audio":
                self.inputDeviceButtons.hoverOn(pos)
            if pygame.mouse.get_pressed()[0] == True and self.eventHandler.mouseWasPressed == False:
                self.eventHandler.mouseWasPressed = True
                if self.highLighter.whatButton.onClick() == True:
                    self.eventHandler.inWhatButton = True        
                if self.startButton.onClick() == 1:
                    try:
                        self.eventHandler.initiateInputMethod()
                        self.inputDeviceError = False
                        self.eventHandler.gameState += 1
                    except:
                        self.eventHandler.inputDeviceError = True
                        print("Error, could not initiate input device")
                if self.pianoButton.onClick() == 1:
                    self.eventHandler.inputMethod = "Piano"
                    self.pianoButton.isSelected = True
                    self.keyboardButton.isSelected = False
                    self.audioButton.isSelected = False
                if self.keyboardButton.onClick() == 1:
                    self.eventHandler.inputMethod = "Keyboard"
                    self.keyboardButton.isSelected = True
                    self.pianoButton.isSelected = False
                    self.audioButton.isSelected = False
                if self.audioButton.onClick() == 1:
                    self.eventHandler.inputMethod = "Audio"
                    self.audioButton.isSelected = True
                    self.pianoButton.isSelected = False
                    self.keyboardButton.isSelected = False
                
                if self.eventHandler.inputMethod == "Piano" or self.eventHandler.inputMethod == "Audio":
                    self.inputDeviceButtons.onClick()

                if self.vanillaButton.onClick() == 1:
                    if self.vanillaButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[0] = 1
                        self.vanillaButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[1] == 1 or self.eventHandler.gameEnharmonic[2] == 1: # Checking if at least 1 enharmonic button is selected
                        self.eventHandler.gameEnharmonic[0] = 0
                        self.vanillaButton.isSelected = False
                if self.sharpsButton.onClick() == 1:
                    if self.sharpsButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[1] = 1
                        self.sharpsButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[0] == 1 or self.eventHandler.gameEnharmonic[2] == 1:
                        self.eventHandler.gameEnharmonic[1] = 0
                        self.sharpsButton.isSelected = False
                if self.flatsButton.onClick() == 1:
                    if self.flatsButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[2] = 1
                        self.flatsButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[0] == 1 or self.eventHandler.gameEnharmonic[1] == 1:
                        self.eventHandler.gameEnharmonic[2] = 0
                        self.flatsButton.isSelected = False
            self.draw()
        if self.eventHandler.inWhatButton == True:
            self.draw()
            if self.eventHandler.whatButtonStage == 0:
                text = ["Here you can pick your method of input"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.keyboardButton.topLeft[0], self.keyboardButton.topLeft[1], self.pianoButton.botRight[0] - self.keyboardButton.topLeft[0], self.pianoButton.botRight[1] - self.pianoButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 1:
                text = ["You can use a computer keyboard", "to practise the names of the notes"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.keyboardButton.topLeft[0], self.keyboardButton.topLeft[1], self.keyboardButton.botRight[0] - self.keyboardButton.topLeft[0], self.keyboardButton.botRight[1] - self.keyboardButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 2:
                text = ["You can connect a MIDI device (such as a piano)", "to practice matching the notes to the keys"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.pianoButton.topLeft[0], self.pianoButton.topLeft[1], self.pianoButton.botRight[0] - self.pianoButton.topLeft[0], self.pianoButton.botRight[1] - self.pianoButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 3:
                text = ["Soon you'll also be able to use your microphone"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.audioButton.topLeft[0], self.audioButton.topLeft[1], self.audioButton.botRight[0] - self.audioButton.topLeft[0], self.audioButton.botRight[1] - self.audioButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 4:
                text = ["You can also add any combination of enharmonics","If you're on keyboard, hold Shift for a Sharp and Alt for a Flat"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.vanillaButton.topLeft[0], self.vanillaButton.topLeft[1], self.flatsButton.botRight[0] - self.vanillaButton.topLeft[0], self.flatsButton.botRight[1] - self.vanillaButton.topLeft[1], text, pos, "above")
            if self.eventHandler.whatButtonStage == 5:
                self.eventHandler.inWhatButton = False 
                self.eventHandler.whatButtonStage = 0

class PickAudioDevice():
    def __init__(self, screen):
        self.screen = screen
        self.eventHandler = EventHandler()

        self.exTopText = "I don't know what input device you want to use"
        self.exTopFontSize = getFontSizeBasedOnSize(self.exTopText, self.screen.get_size()[1] * 1 / 30, "height")
        self.exTopXpos = self.screen.get_size()[0]* 40 / 50
        self.exTopYpos = self.screen.get_size()[1] * 40 / 50
        self.exTop = Text(self.screen, self.exTopText, self.exTopFontSize, self.exTopXpos, self.exTopYpos)

        self.exBotText = "You neither? Try randomly to find which works"
        self.exBotFontSize = getFontSizeBasedOnSize(self.exTopText, self.screen.get_size()[1] * 1 / 30, "height")
        self.exBotXpos = self.screen.get_size()[0] * 40 / 50
        self.exBotYpos = self.screen.get_size()[1] * 42 / 50
        self.exBot = Text(self.screen, self.exBotText, self.exBotFontSize, self.exBotXpos, self.exBotYpos)

        p = pyaudio.PyAudio()
        self.amount = p.get_device_count()
        self.deviceList = []
        for i in range(self.amount):
            dev = p.get_device_info_by_index(i)
            self.deviceList.append(str(i) + " " + str(dev['name']))
        
        self.fontSize = getFontSizeBasedOnSize("TestText", self.screen.get_size()[1]/40, "height")
        self.xPos = self.screen.get_size()[0] * 36 / 50 
        self.yPos = self.screen.get_size()[1] * 16 / 50
        self.deviceButtonList = []
        for i in range(self.amount):
            width = getTextWidth(self.deviceList[i], self.fontSize)
            xPos = self.xPos + width / 2
            yPos = self.yPos + 1.2 * i * self.screen.get_size()[1]/40
            button = Button(self.screen, self.deviceList[i], self.fontSize, xPos, yPos)
            button.rectangle.adjustRect(5)
            button.updateHoverArea()
            self.deviceButtonList.append(button)
        
    def hoverOn(self, pos):
        for i in range(self.amount):
            self.deviceButtonList[i].hoverOn(pos)
    
    def onClick(self):
        for i in range(self.amount):
            if self.deviceButtonList[i].onClick() == True:
                self.deviceButtonList[i].isSelected = True
                self.eventHandler.inputDeviceID = i
                self.eventHandler.inputDeviceError = False
                for j in range(self.amount):
                    if j != i:
                        self.deviceButtonList[j].isSelected = False
                break

    def draw(self):
        self.exTop.draw()
        self.exBot.draw()
        for i in range(self.amount):
            self.deviceButtonList[i].draw()
        
        #get x and y position for buttons

        #get list of audio devices
        #create same amount of buttons on the right location
        #check for onhover and onclick
        #pass the device to the eventhandler
        