import pygame 
import time
from math import ceil
from pathlib import Path


#The following function SHOULD be in PyGame but isn't. It returns the required font size to make a text be a certain size in a certain dimension (width or height). 

def getFontSizeBasedOnSize(text, size, dimension):
    dim = 0
    if dimension == "height":
        dim = 1
    color = (255, 255, 255)
    fontSize = 1
    textSize = [0, 0]
    while textSize[dim] < size:
        fontSize = fontSize + 1
        root = Path(".")
        path = str(root/"resources"/"Marlboro.ttf")
        font = pygame.font.Font(path, fontSize)
        surface = font.render(text, True, color)
        textSize = surface.get_size()
    return fontSize

def getTextWidth(text, fontSize):
    color = (255, 255, 255)
    root = Path(".")
    path = str(root/"resources"/"Marlboro.ttf")
    font = pygame.font.Font(path, fontSize)
    surface = font.render(text, True, color)
    textSize = surface.get_size()
    return textSize[0]


def getScreen(windowHeight, windowWidth, name):
    screen = pygame.display.set_mode((windowWidth, windowHeight))
    pygame.display.set_caption(name)
    return screen

class Position:
    def __init__(self, x, y):
            self.x = x
            self.y = y

class Size:
    def __init__(self, width, height):
        self.width = width
        self.height = height

class Text():
    def __init__(self, screen, text, fontSize, x, y):
        self.text = text
        self.screen = screen
        root = Path(".")
        path = str(root/"resources"/"Marlboro.ttf")
        self.font = pygame.font.Font(path, fontSize)
        self.color = (255, 255, 255)
        #Maybe add dynamic font sizing for fitting on any screen size
        #(Forloop that adjusts fontsize to get it closest to a certain desired textheight)
        self.surface = self.font.render(text, True, self.color)
        
        self.textSize = self.surface.get_size()
        self.position = Position(int(ceil(x - self.textSize[0]/2)), int(ceil(y - self.textSize[1]/2)))
        self.size = Size(self.textSize[0], self.textSize[1])
        self.draw()
    
    def draw(self):
        self.surface = self.font.render(self.text, True, self.color)
        self.screen.blit(self.surface, dest=(self.position.x,self.position.y))

class Rectangle():
    def __init__(self, screen, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width 
        self.height = height
        self.padding = screen.get_size()[0] / 50
        self.lineWidth = 3
        self.color = (255, 255, 255)
        self.backgroundcolor = (0, 0, 0)
        self.screen = screen
        self.position = Position(x - width/2  - self.padding/2, y - height/2 - self.padding/2)
        self.size = Size(width + self.padding, height + self.padding)
        self.rect = [ceil(self.position.x), ceil(self.position.y), ceil(self.size.width), ceil(self.size.height)]

    def draw(self):
        pygame.draw.rect(self.screen, self.color, self.rect, self.lineWidth)
        pygame.draw.rect(self.screen, self.backgroundcolor, self.rect, 0)
    
    def adjustRect(self, padding):
        self.padding = padding 
        self.position = Position(self.x - self.width/2  - self.padding/2, self.y - self.height/2 - self.padding/2)
        self.size = Size(self.width + self.padding, self.height + self.padding)
        self.rect = [ceil(self.position.x), ceil(self.position.y), ceil(self.size.width), ceil(self.size.height)]

class Button():
    def __init__(self, screen, text, fontSize, x, y):
        self.hover = False
        self.text = Text(screen, text, fontSize, x, y)
        self.rectangle = Rectangle(screen, x, y, self.text.size.width, self.text.size.height)
        self.topLeft = [self.rectangle.position.x, self.rectangle.position.y]
        self.botRight = [self.rectangle.position.x + self.rectangle.size.width, self.rectangle.position.y + self.rectangle.size.height]
        self.isSelected = False
        self.draw()
        
    def draw(self):
        if self.isSelected == True:
            self.rectangle.backgroundcolor = (50, 50, 50)
        self.rectangle.draw()
        self.text.draw()
    
    def mouseOn(self, pos):
        if pos[0] > self.topLeft[0] and pos[0] < self.botRight[0] and pos[1] > self.topLeft[1] and pos[1] < self.botRight[1]:
            return True
        else:
            return False
    
    def hoverOn(self, pos):
        self.hover = self.mouseOn(pos)
        if self.hover:
            self.rectangle.backgroundcolor = (100, 100, 100)
        else:
            self.rectangle.backgroundcolor = (0, 0, 0)

    def onClick(self):
        if self.hover == True: return True
        else: return False
    
    def updateHoverArea(self):
        self.topLeft = [self.rectangle.position.x, self.rectangle.position.y]
        self.botRight = [self.rectangle.position.x + self.rectangle.size.width, self.rectangle.position.y + self.rectangle.size.height]
    
    
class Timer:
    def __init__(self):
        self._start_time = None

    def start(self):
        if self._start_time is not None:
            print("Timer is running. Use .stop() to stop it")

        self._start_time = time.perf_counter()

    def stop(self):
        if self._start_time is None:
            print("Timer is not running. Use .start() to start it")

        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        return elapsed_time