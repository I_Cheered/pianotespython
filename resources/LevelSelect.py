import os
from pathlib import Path
from math import ceil, floor

from resources.MiscResources import *
from resources.BaseUIClass import *
from resources.DataIOClass import *

class Level(Button):
    def __init__(self, screen, octaves, speed, amount, percentage, locked, x, y):
        self.x = x 
        self.y = y
        self.screen = screen
        self.octaves = octaves
        self.speed = speed
        self.amount = amount
        self.percentage = percentage
        self.locked = locked
        self.highscore = 0
        self.text = str(self.octaves+1) + " - " + str(self.speed) + " | " + str(self.percentage) + "%"
        self.fontSize = getFontSizeBasedOnSize(self.text, self.screen.get_size()[0] * 1 / 7, "width")
        self.saveData = [self.octaves, self.speed, self.amount, self.percentage, self.locked]
        self.updateText()
        
    
    def updateLockedColor(self):
        if self.locked == True:
            self.rectangle.color = (100, 100, 100)
            self.text.color = (100, 100, 100)
        if self.locked == False:
            self.rectangle.color = (255, 255, 255)
            self.text.c = (255, 255, 255)

    def updateText(self):
        if self.highscore == self.amount:
            self.text = str(self.octaves+1) + " - " + str(self.speed) + " | " + str(self.percentage) + "%"
        else:
            self.text = str(self.octaves+1) + " - " + str(self.speed) + " | " + str(self.highscore) + "/" + str(self.amount)
        super().__init__(self.screen, self.text, self.fontSize, self.x, self.y)
    
    def dataToVars(self):
        self.octaves = self.saveData[0]
        self.speed = self.saveData[1]
        self.amount = self.saveData[2]
        self.percentage = self.saveData[3]
        self.locked = self.saveData[4]
        self.highscore = self.saveData[5]
        self.updateText()

    def varsToData(self):
        self.saveData = [self.octaves, self.speed, self.amount, self.percentage, self.locked, self.highscore]
        self.updateText()

class LevelSelect(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.dataIO = DataIO()

        self.titleText = "Level Select"
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1 / 8, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 10
        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)

        self.buttonText = "Back"
        self.buttonFontSize = getFontSizeBasedOnSize(self.buttonText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width / 10
        self.buttonYpos = self.height / 10
        self.backButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)

        self.unlockText = "UnlockAll"
        self.unlockFontSize = getFontSizeBasedOnSize(self.unlockText, self.height * 1 / 13, "height")
        self.unlockXpos = self.width * 3 / 10
        self.unlockYpos = self.height / 10
        self.unlockButton = Button(self.screen, self.unlockText, self.unlockFontSize, self.unlockXpos, self.unlockYpos)

        self.octaveRange = 4 #Even though there are 7 octaves, the middle 4 are used like 98% of the time
        self.speedRange = 6
        self.levelButtonArray = self.getLevelButtonArray(self.octaveRange, self.speedRange)
        self.levelButtonSaveDataArray = self.createLevelButtonSaveDataArray()
    
    def draw(self):
        self.title.draw()
        self.backButton.draw()
        #self.unlockButton.draw()
        self.highLighter.whatButton.draw()
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                self.levelButtonArray[i][j].updateLockedColor()
                self.levelButtonArray[i][j].draw()
    
    def createLevelButtonSaveDataArray(self):
        verticalArray = []
        for i in range(self.octaveRange):
            horizontalArray = []
            for j in range(self.speedRange):
                horizontalArray.append(self.levelButtonArray[i][j].saveData)
            verticalArray.append(horizontalArray)
        return verticalArray
    
    def levelButtonDataToSave(self):
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                self.levelButtonSaveDataArray[i][j] = self.levelButtonArray[i][j].saveData
        self.dataIO.write(self.levelButtonSaveDataArray)

    def saveToLevelButtons(self):
        for i in range(self.octaveRange):
            for j in range(self.speedRange):
                self.levelButtonArray[i][j].saveData = self.levelButtonSaveDataArray[i][j]
                self.levelButtonArray[i][j].dataToVars()

    def getLevelButtonArray(self, maxOctaves, maxSpeed):
        octaveArray = []
        for i in range(maxOctaves):
            speedArray = []
            for j in range(maxSpeed): 
                speedArray.append(Level(self.screen, i, j, 15 + 15 * j, 0, True, self.width/10 + self.width * j / 5, self.height * 3 / 10 + self.height * i / 5))
            octaveArray.append(speedArray)
        octaveArray[0][0].locked = False
        levelButtonArray = octaveArray
        self.eventHandler.gameCompleted = 0
        self.eventHandler.gameMistakes = 0
        return levelButtonArray
        
    def isActive(self):
        self.eventHandler.inLevelSelect = True
        if self.eventHandler.inGame == True:
            self.eventHandler.inGame = False
        pos = pygame.mouse.get_pos()
        if self.eventHandler.inWhatButton == False:
            if self.eventHandler.inMainMenu == True:
                self.eventHandler.inMainMenu = False
                # if os.path.exists(self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"):
                #     self.levelButtonSaveDataArray = self.dataIO.read()
                #     self.saveToLevelButtons()
                root = Path(".")
                name = str(self.eventHandler.inputMethod) + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
                path = root/"SaveData"/name
                if os.path.exists(path):
                    self.levelButtonSaveDataArray = self.dataIO.read()
                    self.saveToLevelButtons()
                else:
                    self.levelButtonArray = self.getLevelButtonArray(self.octaveRange, self.speedRange)
                    self.levelButtonSaveDataArray = self.createLevelButtonSaveDataArray()
            # if self.eventHandler.inGame == True:
            #     self.eventHandler.inGame = False
            #     self.levelButtonDataToSave()
            if self.eventHandler.updateLevelData == True:
                self.eventHandler.updateLevelData = False
                if self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].percentage < ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100):
                    self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].percentage = ceil(self.eventHandler.gameCompleted / (self.eventHandler.gameMistakes + self.eventHandler.gameCompleted) * 100)
                if self.eventHandler.gameCompleted > self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].highscore:
                    self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].highscore = self.eventHandler.gameCompleted
                if self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].percentage > 75 and self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].highscore == self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].amount:
                    if self.eventHandler.levelID[1] == self.speedRange - 2:
                        self.levelButtonArray[self.eventHandler.levelID[0] + 1][0].locked = False
                    else:
                        self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1] + 1].locked = False
                self.levelButtonArray[self.eventHandler.levelID[0]][self.eventHandler.levelID[1]].updateText()
                for i in range(self.octaveRange):
                    for j in range(self.speedRange):
                        self.levelButtonArray[i][j].varsToData()
                self.levelButtonDataToSave()     
        
            self.backButton.hoverOn(pos)
            #self.unlockButton.hoverOn(pos)
            self.highLighter.whatButton.hoverOn(pos)
            for i in range(self.octaveRange):
                for j in range(self.speedRange):
                    if self.levelButtonArray[i][j].locked == False:
                        self.levelButtonArray[i][j].hoverOn(pos)

            if pygame.mouse.get_pressed()[0] == True and self.eventHandler.mouseWasPressed == False:
                self.eventHandler.mouseWasPressed == True
                if self.highLighter.whatButton.onClick() == True:
                    self.eventHandler.inWhatButton = True
                elif self.backButton.onClick() == 1:
                    self.eventHandler.gameState -= 1
                elif self.unlockButton.onClick() == 1:
                    for i in range(self.octaveRange):
                        for j in range(self.speedRange):
                            self.levelButtonArray[i][j].locked = False
                else:
                    for i in range(self.octaveRange):
                        for j in range(self.speedRange):
                            if self.levelButtonArray[i][j].onClick() and self.eventHandler.mouseWasPressed == False:
                                self.eventHandler.mouseWasPressed = True

                                self.eventHandler.gameSpeed = self.levelButtonArray[i][j].speed
                                self.eventHandler.gameOctaves = self.levelButtonArray[i][j].octaves
                                self.eventHandler.gameAmount = self.levelButtonArray[i][j].amount
                                self.eventHandler.updateLevelID()

                                self.eventHandler.gameState += 1
                                self.eventHandler.inLevelSelect = False
            self.draw()
        if self.eventHandler.inWhatButton == True:
            self.draw()
            if self.eventHandler.whatButtonStage == 0:
                text = ["You can press an unlocked level to play it", "The numbers display the Range, Speed and High-Score"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.levelButtonArray[0][0].topLeft[0], self.levelButtonArray[0][0].topLeft[1], self.levelButtonArray[0][0].botRight[0] - self.levelButtonArray[0][0].topLeft[0], self.levelButtonArray[0][0].botRight[1] - self.levelButtonArray[0][0].topLeft[1], text, pos, "center")
            if self.eventHandler.whatButtonStage == 1:
                text = ["The speed of the notes increases with horizontal levels"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.levelButtonArray[0][0].topLeft[0], self.levelButtonArray[0][0].topLeft[1], self.levelButtonArray[0][self.speedRange-1].botRight[0] - self.levelButtonArray[0][0].topLeft[0], self.levelButtonArray[0][self.speedRange-1].botRight[1] - self.levelButtonArray[0][0].topLeft[1], text, pos, "center")
            if self.eventHandler.whatButtonStage == 2:
                text = ["And the range (number of octaves) from which the", "random notes are drawn increases vertically"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.levelButtonArray[0][0].topLeft[0], self.levelButtonArray[0][0].topLeft[1], self.levelButtonArray[self.octaveRange-1][0].botRight[0] - self.levelButtonArray[0][0].topLeft[0], self.levelButtonArray[self.octaveRange-1][0].botRight[1] - self.levelButtonArray[0][0].topLeft[1], text, pos, "center")
            if self.eventHandler.whatButtonStage == 3:
                text = ["Everytime you complete a level your progress is saved so", "you can quit and come back later without losing progress"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(0, 0, -35, -35, text, pos, "center")
            if self.eventHandler.whatButtonStage == 4:
                text = ["Different enharmonic combinations and input methods are", "saved in different files so your progress doesn't mix"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(0, 0, -35, -35, text, pos, "center")
            if self.eventHandler.whatButtonStage == 5:
                self.eventHandler.inWhatButton = False 
                self.eventHandler.whatButtonStage = 0